@extends('layouts.dashboard')
@section('content')

<div class="container">
     <div class="row justify-content-center">
         <div class="col-md-8">
             <div class="card">
                   <div class="card-body">
                   <h1>Hola Recruiter , you have applicants!!</h1>
                   <h3>{{$candidate}} has applied for {{$job}}</h3>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection