@extends('layouts.dashboard')
@section('content')

<div class="container">
     <div class="row justify-content-center">
         <div class="col-md-8">
             <div class="card">
                 
                   <div class="card-body">
                   <h1>Congratulations!!</h1>
                   <h3>You have successfully applied to {{$job}}</h3>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection