@extends('layouts.dashboard')
@section('content')

<div class="container">
     <div class="row justify-content-center">
         <div class="col-md-8">
             <div class="card">
                 <div class="card-header">Your OTP</div>
                   <div class="card-body">
                    <h1 class="display-4">{{$otp}}</h1>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection