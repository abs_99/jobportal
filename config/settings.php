<?php

return [
    "genders"=>[
        "Male"=>0,
        "Female"=>1
    ],
    "types"=>[
        "Candidate"=>0,
        "Recruiter"=>1,
        "Admin"=>2
    ]
];