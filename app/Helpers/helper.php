<?php

use Illuminate\Support\Str;

function getClassName($name)
{
return str_replace('_', ' ', Str::snake(class_basename($name)));
}