<?php

namespace App\Services;

use Illuminate\Support\Arr;
use App\Validators\Validator;
use App\Validators\AuthValidator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Repositories\Contracts\UserRepository;
use App\Exceptions\InvalidCredentialsException;

class AuthService
{
    protected $user_repo;

    public function __construct(UserRepository $user_repo)
    {
        $this->user_repo = $user_repo;
    }

    public function signup($inputs, AuthValidator $validator)
    {
        $validator->fire($inputs, "signup");

        $user = $this->user_repo->create(Arr::only(
            $inputs,
            ['first_name', 'last_name', 'email', 'password', 'dob', 'gender', 'type']
        ));

        $user['token'] = JWTAuth::fromUser($user);

        return $user;
    }

    public function signin($inputs, AuthValidator $validator)
    {  
        $validator->fire($inputs, "signin");
       
        $user = $this->user_repo->firstWhere('email', Arr::get($inputs, 'email'));

        if (Hash::check(Arr::get($inputs, 'password'), $user->password)) {

            $user['token'] = JWTAuth::fromUser($user);
            return $user;
        }
        throw new InvalidCredentialsException("Invalid Credentials");
    }

    public function logout()
    {
        
        try {
            JWTAuth::invalidate(JWTAuth::getToken());
            return;
        } catch (JWTException $exception) {
            throw $exception;
        }
    }
}
