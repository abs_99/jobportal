<?php

namespace App\Services;

use Illuminate\Support\Arr;
use App\Validators\JobValidator;
use App\Repositories\Contracts\JobRepository;
use Illuminate\Validation\UnauthorizedException;

class JobService
{
    protected $job_repo;

    public function __construct(JobRepository $job_repo)
    {
        $this->job_repo = $job_repo;
    }

    public function getJobs()
    {
        $jobs = $this->job_repo->all();
        return $jobs;
    }
    public function getApplicants($job_uuid)
    {
        $job = $this->job_repo->firstWhere('uuid',$job_uuid);
        if(auth()->user()->id!==$job->recruiter_id){
            throw new UnauthorizedException("These job is not posted by you");
        }
        return $job->applicants;
    }

    public function postJob($inputs,JobValidator $validator)
    {  
        $validator->fire($inputs,'postJob');
        $job = $this->job_repo->create(Arr::only($inputs,['title','description','recruiter_id']));  
        return $job;        
    }
}
