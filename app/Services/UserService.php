<?php

namespace App\Services;

use App\Repositories\Contracts\UserRepository;

class UserService
{
    protected $user_repo;

    public function __construct(UserRepository $user_repo)
    {
        $this->user_repo = $user_repo;
        $this->user = auth()->user();
    }

    public function getJobsPosted()
    {
        return $this->user->jobsPosted;
    }

    public function getApplications()
    {
        return $this->user->applications;
    }
}
