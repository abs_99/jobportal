<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Validators\AuthValidator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;


class ForgotPasswordService
{

    public function __construct()
    {
    }

    public function forgotPassword($input,  AuthValidator $validator)
    {
        // $slug = Password::sendResetLink(Arr::only($input, ['email']), function ($user, $password) {
        //     $user->password = $password;
        //     $user->save();
        // });
        $validator->fire($input,'forgotPassword');
        $otp = uniqid();
        $email = $input['email'];
        
        DB::table('password_resets')->insert(
            ['email' => $email, 'otp' => $otp, 'created_at' => Carbon::now()]
        );

        Mail::send('customauth.verify', ['otp' => $otp], function ($message) use ($input) {
            $email = $input['email'];
            $message->to($email);
            $message->subject('Reset Password Notification');
        });
        return;
    }

    public function resetPassword($input, AuthValidator $validator)
    {   
        $validator->fire($input,'resetPassword');
        $email = $input['email'];
        $otp = $input['otp'];
        $password = $input['password'];
        $updatePassword = DB::table('password_resets')->where(['email' => $email, 'otp' => $otp])->first();
        
        $user = User::where('email', $email)->update(['password' => Hash::make($password)]);
      
        DB::table('password_resets')->where(['email'=> $email])->delete();
    }
}
