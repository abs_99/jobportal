<?php

namespace App\Services;

use Illuminate\Support\Facades\Mail;
use App\Exceptions\DuplicateRecordException;
use App\Repositories\Contracts\JobRepository;
use App\Repositories\Contracts\ApplicationRepository;
use App\Repositories\Contracts\UserRepository;

class ApplicationService
{
    protected $application_repo, $job_repo;

    public function __construct(ApplicationRepository $application_repo, JobRepository $job_repo, UserRepository $user_repo)
    {
        $this->application_repo = $application_repo;
        $this->job_repo = $job_repo;
        $this->user_repo = $user_repo;
    }

    public function applyJob($job_uuid)
    {
        $user = auth()->user();
        $job = $this->job_repo->firstWhere('uuid',$job_uuid);
        $user_applications = $user->applications()->where('jobs.uuid', $job_uuid)->first();
        
        if($user_applications) {
            throw new DuplicateRecordException('Already applied to job');
        }

        $application = $this->application_repo->create([
            'user_id' => $user->id,
            'job_id' =>  $job->id
        ]);

        Mail::send('application.candidate', ['job' => $job->title], function ($message){
            $email = auth()->user()->email;
            $message->to($email);
            $message->subject('Successfully Applied');
        });

        $email = $this->user_repo->firstWhere('id',$job->recruiter_id)->email;
        Mail::send('application.recruiter', ['candidate' => auth()->user()->first_name, 'job' => $job->title], function ($message) use ($email){
            $message->to($email);
            $message->subject('Candidate Applied !!');
        });
        return $application;
    }
}
