<?php

namespace App\Validators;

class JobValidator extends Validator
{
    /**
     * Validation rules.
     *
     * @param  string $type
     * @param  array $data
     * @return array
     */

    protected function rules($data, $type)
    {
        $rules = [];

        switch ($type) {
            case 'postJob':
                $rules = [
                    'title' => 'required',
                    'description' => 'required|min:10',
                    'recruiter_id' => 'required'
                ];
                break;
            default:
                return [];
        }

        return $rules;
    }

    protected function messages($type)
    {
        switch ($type) {
            case 'postJob':
                return [
                    'title.required' => 'Please provide title',
                    'description.required' => 'Please provide description',
                    'description.min'   => 'Description should be of min 10 characters',
                    'recruiter.required' => 'Recruiter ID is required'
                ];
                break;

            default:
                return [];
        }
    }
}
