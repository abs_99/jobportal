<?php

namespace App\Validators;

class ApplicationValidator extends Validator
{
    /**
     * Validation rules.
     *
     * @param  string $type
     * @param  array $data
     * @return array
     */

    protected function rules($data, $type)
    {
        $rules = [];

        switch ($type) {
            case 'applyJob':
                $rules = [
                    'job_id' => 'required',
                    'user_id' => 'required'
                ];
                break;
        }

        return $rules;
    }

    protected function messages($type)
    {
        switch ($type) {
            case 'applyJob':
                return [
                    'job_id.required' => 'Invalid Params : Job ID Required ',
                    'user_id.required' => 'Invalid Params : User ID Required'
                ];
                break;

            default:
                return [];
        }
    }
}
