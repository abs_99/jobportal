<?php

namespace App\Validators;

class AuthValidator extends Validator
{
    /**
     * Validation rules.
     *
     * @param  string $type
     * @param  array $data
     * @return array
     */

    protected function rules($data, $type)
    {
        $rules = [];

        switch ($type) {
            case 'signup':
                $rules = [
                    'email' => 'required|email|unique:users',
                    'password' => 'required|confirmed|min:6|max:100',
                    'first_name' => 'required|string|max:255',
                    'last_name' => 'required|string|max:255',
                    'gender' => 'required|integer|in:' . implode(',', array_values(config('settings.genders'))),
                    'type' => 'required|integer|in:' . implode(',', array_values(config('settings.types'))),
                    // 'phone_number' => 'digits:10|unique:users',
                    // 'app_source' => 'sometimes',
                    // 'company_name' => 'sometimes|required_with:app_source|string|max:255',
                    // 'company_id' => 'sometimes|string',
                ];
                break;
            case 'signin':
                $rules = [
                    'email' => 'required|email',
                    'password' => 'required|min:6|max:100',
                ];
                break;
            case 'logout':
                $rules = [
                    'token' => 'required',
                ];
            case 'forgotPassword':
                    $rules = [
                        'email' => 'required|email'
                    ];
                break;
            case 'resetPassword':
                    $rules = [
                        'email' => 'required|email',
                        'password' => 'required|confirmed'
                    ];
                break;
        }

        return $rules;
    }

    protected function messages($type)
    {
        switch ($type) {
            case 'signup':
                return [
                    'email.required' => 'Please provide email',
                    'first_name.required' => 'Please provide First Name',
                    
                ];
                break;

            default:
                return [];
        }
    }
}
