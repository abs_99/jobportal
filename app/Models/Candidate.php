<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    use HasFactory;

    public function applications()
    {
        return $this->hasManyThrough(Job::class, Application::class,'user_id','id','id','job_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
