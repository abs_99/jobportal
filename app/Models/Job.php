<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    use HasFactory;
    // public function candidate()
    // {
    //     return $this->belongsTo(Candidate::class);
    // }
    protected $fillable = ['title','description','recruiter_id'];
    public function recruiter()
    {
        return $this->belongsTo(User::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class,'recruiter_id');
    }
    public function applicants()
    {
        return $this->hasManyThrough(User::class, Application::class,'job_id','id','id','user_id');
    }
}
