<?php

namespace App\Exceptions;

use Throwable;
use App\Traits\RestTrait;
use App\Traits\RestExceptionHandlerTrait;
use App\Exceptions\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException as BaseValidationException;

class Handler extends ExceptionHandler
{
    use RestTrait, RestExceptionHandlerTrait;

    protected $dontReport = [];

    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $exception)
    {
        if ($this->isApiCall($request) || $this->isAjaxCall($request) || $request->wantsJson()) {
            return $this->renderForRest($request, $exception);
        } else {
            return $this->renderForWeb($request, $exception);
        }

        return parent::render($request, $exception);
    }

    /**
     * Render an exception into an HTTP response for a RESTful request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $e
     * @return Illuminate\Http\JsonResponse
     */
    protected function renderForRest($request, $e)
    {
        switch ($e) {
            case $e instanceof InvalidCredentialsException:
                return response()->error($e->getMessage(), 400);

            case $e instanceof BaseValidationException:
                return response()->error($e->errors()->toArray(), 422);

            case $e instanceof ModelNotFoundException:
                return response()->error($e->getMessage(), 404);

            case $e instanceof DuplicateRecordException:
                return response()->error($e->getMessage(), 409);

            case $e instanceof UnauthorizedException:
                return response()->error($e->getMessage(), 401);

            default:
                return $this->renderRestException($request, $e);
        }
    }

    /**
     * @param $request
     * @param Throwable $e
     * @return $this|\Symfony\Component\HttpFoundation\Response
     */
    protected function renderForWeb($request, Throwable $e)
    {
        switch ($e) {
          
            default:
                return parent::render($request, $e);
        }
    }
}
