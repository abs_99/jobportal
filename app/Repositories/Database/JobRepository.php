<?php

namespace App\Repositories\Database;

use App\Models\Job;
use App\Traits\DatabaseRepositoryTrait;
use App\Repositories\Contracts\JobRepository as JobRepositoryContract;

class JobRepository implements JobRepositoryContract{
    use DatabaseRepositoryTrait;
    protected $model=Job::class;
    
}