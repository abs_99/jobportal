<?php

namespace App\Repositories\Database;


use App\Models\Application;
use App\Traits\DatabaseRepositoryTrait;
use App\Repositories\Contracts\ApplicationRepository as ApplicationRepositoryContract;

class ApplicationRepository implements ApplicationRepositoryContract{
    use DatabaseRepositoryTrait;
    protected $model=Application::class;   
}