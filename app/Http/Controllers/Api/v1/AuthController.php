<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\User;
use Illuminate\Http\Request;
use App\Services\AuthService;
use App\Transformers\UserTransformer;

class AuthController extends Controller
{
    protected $auth_service;

    public function __construct(AuthService $auth_service)
    {
        $this->auth_service = $auth_service;
    }

    public function signup()
    {
       $inputs = request()->all();
     
       $user = app()->call([$this->auth_service, "signup"], [
            "inputs"=>$inputs
       ]);
    
        return response()->success($this->getTransformedData($user, new UserTransformer));
    }

    public function signin()
    {
        $inputs = request()->all();   
            
        $user = app()->call([$this->auth_service, "signin"], [
             "inputs"=>$inputs
        ]);

        return response()->success($this->getTransformedData($user, new UserTransformer));
    }

    public function logout()
    {
        $inputs = request()->all();
        app()->call([$this->auth_service, "logout"], [
             "inputs"=>$inputs
        ]);

        return response()->success('User successfully signed out',201);
    }

}
