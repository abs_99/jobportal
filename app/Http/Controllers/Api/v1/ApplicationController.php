<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Services\ApplicationService;
use App\Transformers\ApplicationTransformer;

class ApplicationController extends Controller
{
    protected $application_service;

    public function __construct(ApplicationService $application_service)
    {
        $this->application_service = $application_service;
    }
    public function applyJob($job_uuid)
    {
        app()->call([$this->application_service, "applyJob"], [ 
            "job_uuid" => $job_uuid
        ]);

        return response()->success('',201);
    }
}
