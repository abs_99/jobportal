<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Services\ForgotPasswordService;

class ForgotPasswordController extends Controller
{
    protected $forgotPassword_service;

    public function __construct(ForgotPasswordService $forgotPassword_service)
    {
        $this->forgotPassword_service = $forgotPassword_service;
    }
    public function forgotPassword()
    {
        $input = request()->all();
        app()->call([$this->forgotPassword_service, "forgotPassword"], [
          "input" => $input  
        ]);

        return response()->success('',201);
       
    }
    
    public function resetPassword()
    {
        $input = request()->all();
        app()->call([$this->forgotPassword_service, "resetPassword"], [
          "input" => $input  
        ]);

        return response()->success('',201);
       
    }
}
