<?php


namespace App\Http\Controllers\Api\v1;

use App\Services\JobService;
use Illuminate\Http\Request;
use App\Transformers\JobTransformer;
use App\Transformers\UserTransformer;

class JobContoller extends Controller
{
    protected $job_service;

    public function __construct(JobService $job_service)
    {
        $this->job_service = $job_service;
    }
    public function getJobs()
    {
        $jobs = app()->call([$this->job_service, "getJobs"], []);

        return response()->success($this->getTransformedData($jobs, new JobTransformer));
    }

    public function getApplicants($job_uuid)
    {   
        $applicants = app()->call([$this->job_service, "getApplicants"], [
            "job_uuid"=>$job_uuid]
        );

        return response()->success($this->getTransformedData($applicants, new UserTransformer));
    }

    public function postJob()
    {   
        $inputs = request()->all();
        $inputs['recruiter_id'] = auth()->user()->id;
        
        $job = app()->call([$this->job_service, "postJob"], [
            "inputs"  => $inputs
        ]);

        return response()->success($this->getTransformedData($job, new JobTransformer));
    }
}
