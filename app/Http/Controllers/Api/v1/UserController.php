<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Services\UserService;
use App\Transformers\JobTransformer;


class UserController extends Controller
{
    protected $user_service;

    public function __construct(UserService $user_service)
    {
        $this->user_service = $user_service;
    }

    public function getApplications()
    {

        $jobs = app()->call([$this->user_service, "getApplications"], []);

        return response()->success($this->getTransformedData($jobs, new JobTransformer));
    }

    public function getJobsPosted()
    {

        $jobs = app()->call([$this->user_service, "getJobsPosted"], []);

        return response()->success($this->getTransformedData($jobs, new JobTransformer));
    }

    public function applyJob($user_id, $job_id)
    {
        $job = app()->call([$this->user_service, "applyJob"], [
            "user_id" => $user_id,
            "job_id"  => $job_id
        ]);

        return response()->success($this->getTransformedData($job, new JobTransformer));
    }

    

}
