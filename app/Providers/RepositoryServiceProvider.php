<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \App\Repositories\Contracts\UserRepository::class,
            \App\Repositories\Database\UserRepository::class,
        );

        $this->app->bind(
            \App\Repositories\Contracts\JobRepository::class,
            \App\Repositories\Database\JobRepository::class
        );

        $this->app->bind(
            \App\Repositories\Contracts\ApplicationRepository::class,
            \App\Repositories\Database\ApplicationRepository::class
        );
    }
}
