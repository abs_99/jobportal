<?php

namespace App\Providers;

use App\Models\Job;
use App\Models\User;
use App\Models\Application;
use App\Observers\JobObserver;
use App\Observers\UserObserver;
use Illuminate\Support\Facades\Event;
use App\Observers\ApplicationObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        Job::observe(JobObserver::class);
        Application::observe(ApplicationObserver::class);
    }
}
