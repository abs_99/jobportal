<?php

namespace App\Providers;

use Response;
use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('success', function ($data, $status = 200, $code = null) {
            $code = is_null($code) ? $status : $code;

            return Response::json([
                'success'  => true,
                'code' => $code,
                'data' => $data
            ], $status);
        });

        Response::macro('error', function ($errors, $status = 400, $code = null) {
            $code = is_null($code) ? $status : $code;

            $response = [
                'success'  => false,
                'code' => $code,
            ];

            if (is_array($errors)) {
                if ($meta = Arr::get($errors, 'meta')) {
                    $response += [
                        'renderable' => Arr::get($meta, 'renderable'),
                        'message' => Arr::get($meta, 'message'),
                    ];
                } else {
                    $response += [
                        'errors' => $errors,
                    ];
                }
            }

            if (is_string($errors)) {
                $response += [
                    'message' => $errors,
                ];
            }

            return Response::json($response, $status);
        });

        Response::macro('withMeta', function ($data) {
			return Response::json([
                'code' => 200,
                'data' => Arr::except($data, ['meta']),
                'meta' => Arr::get($data, 'meta'),
			]);
        });
        
        //To be used for responses with no body content
        Response::macro('noContent', function() {
            return Response::make("", 204);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
