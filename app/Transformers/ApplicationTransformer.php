<?php

namespace App\Transformers;
use App\Models\Application;

class ApplicationTransformer extends Transformer
{

    /**
     * constructor
     * @return null
     */
    public function __constructor()
    {
        //
    }

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include
     *
     * @var array 
     */

    protected $defaultIncludes = [];
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Application $application)
    {
        
        return array_merge([
            'id'=>$application->id,
            'user_id' => $application->user_id,
            'job_id' => $application->job_id,
            'uuid' => $application->uuid
            
        ], $this->getTransformedTimestampsArr($application));
    }
}
