<?php

namespace App\Transformers;

use App\Models\User;

class UserTransformer extends Transformer
{

    /**
     * constructor
     * @return null
     */
    public function __constructor()
    {
        //
    }

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include
     *
     * @var array 
     */

    protected $defaultIncludes = [];
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(User $user)
    {
        $token = $user->token ? ['token' => $user->token] : [];

        return array_merge([
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'gender' => $user->gender == 0 ? 'Male' : 'Female',
            'email' => $user->email,
            'type' => $user->type,
            'dob' => $user->dob,
            'uuid' => $user->uuid
        ], $token, $this->getTransformedTimestampsArr($user));
    }
}
