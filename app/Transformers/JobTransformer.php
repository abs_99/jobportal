<?php

namespace App\Transformers;

use App\Models\Job;
use App\Models\User;

class JobTransformer extends Transformer
{

    /**
     * constructor
     * @return null
     */
    public function __constructor()
    {
        //
    }

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include
     *
     * @var array 
     */

    protected $defaultIncludes = [];
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Job $job)
    {
        
        return array_merge([
            'id'=>$job->id,
            'title' => $job->title,
            'description' => $job->description,
            'recruiter_id' => $job->recruiter_id,
            'uuid' => $job->uuid
            
        ], $this->getTransformedTimestampsArr($job));
    }
}
