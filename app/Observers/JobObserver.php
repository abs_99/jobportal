<?php

namespace App\Observers;

use App\Models\Job;
use Illuminate\Support\Str;

class JobObserver
{
    /**
     * Handle the Job "creating" event.
     *
     * @param  \App\Models\Job  $job
     * @return void
     */
    public function creating(Job $job)
    {   
        $job->uuid = (string) Str::uuid();
    }

}
