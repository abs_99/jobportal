<?php

namespace App\Observers;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */

    public function creating(User $user)
    {
        $user->uuid = (string) Str::uuid();
        $user->password = Hash::make($user->password);
    }
}
