<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\v1\JobContoller;
use App\Http\Controllers\Api\v1\AuthController;
use App\Http\Controllers\Api\v1\UserController;
use App\Http\Controllers\Api\v1\ApplicationController;
use App\Http\Controllers\Api\v1\ForgotPasswordController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/signup', [AuthController::class, 'signup']);
Route::post('/signin', [AuthController::class, 'signin']);
Route::group(['middleware'=>'jwt.auth'], function(){
    Route::get('/logout', [AuthController::class, 'logout']);
});

Route::group(['middleware'=>['jwt.auth','recruiter']], function(){
    Route::get('/jobs', [UserController::class, 'getJobsPosted']);
    Route::get('/{job_uuid}/applicants', [JobContoller::class, 'getApplicants']);
    Route::post('/post', [JobContoller::class, 'postJob']);
});

Route::group(['middleware'=>['jwt.auth','candidate']], function(){
   Route::get('/applications', [UserController::class, 'getApplications']);
   Route::post('/{job_uuid}/apply', [ApplicationController::class, 'applyJob']);
   Route::get('/alljobs', [JobContoller::class, 'getJobs']);
    
});


Route::post('/forgot_password', [ForgotPasswordController::class, 'forgotPassword']);
Route::post('/reset_password', [ForgotPasswordController::class, 'resetPassword']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
